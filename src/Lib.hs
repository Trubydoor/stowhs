{-# LANGUAGE TemplateHaskell, GADTs, LambdaCase, BlockArguments, TypeOperators, DataKinds, ScopedTypeVariables, FlexibleContexts, PolyKinds, OverloadedStrings #-}
module Lib
    ( linkFiles, stowerToOutput
    ) where
import Polysemy
import Polysemy.Output
import Data.Text ( pack, Text )
import System.Directory (createFileLink, listDirectory)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data Stower m a where
  LinkFile :: FilePath -> FilePath -> Stower m ()

makeSem ''Stower

stowerToOutput :: Member (Output Text) r => Sem (Stower ': r) a -> Sem r a
stowerToOutput = interpret \case
  LinkFile src dest -> output $ "ln -s " <> pack src <> " " <> pack dest

stowerToIO :: Member (Embed IO) r => Sem (Stower ': r) a -> Sem r a
stowerToIO = interpret \case
  LinkFile src dest -> embed $ createFileLink src dest

linkFiles :: Members [Stower, Embed IO] r => FilePath -> FilePath -> Sem r ()
linkFiles dir destDir = do
  files <- embed $ listDirectory dir
  mapM_ (\f -> linkFile f (destDir <> "/" <> f)) files
