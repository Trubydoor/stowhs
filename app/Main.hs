{-# LANGUAGE ScopedTypeVariables, TypeApplications, ApplicativeDo, RecordWildCards #-}
module Main where

import Lib
import Data.Text
import System.Directory (createFileLink, doesDirectoryExist, getCurrentDirectory)
import Options.Applicative

import Polysemy.Output
import Polysemy (runFinal, run, embedToFinal)
import Polysemy.IO (embedToMonadIO)
import Lib (stowerToOutput)
import qualified Data.Text.IO as Text
import System.Environment (lookupEnv)
import Data.Maybe (fromMaybe)

data Options = Options { dryRun :: Bool
                       , delete :: Bool
                       , directory :: FilePath }

options :: Maybe FilePath -> Parser Options
options destDir = do
  dryRun <- switch (long "no"
         <> short 'n'
         <> help "Do not perform any operations that modify the filesystem; merely show what would happen.")
  delete <- switch (long "delete"
             <> short 'D'
             <> help "Unstow the packages that follow this option from the target directory rather than installing them.")
  directory <- strOption $ long "dir" <> short 'd' <> value (fromMaybe ".." destDir)
  pure $ Options {..}

doDryRun :: FilePath -> IO ()
doDryRun destDir = do
  (lst, _) <- runFinal . embedToFinal @IO . runOutputList . stowerToOutput $ linkFiles "." destDir
  mapM_ Text.putStrLn lst

opts :: Options -> IO ()
opts (Options True True _) = putStrLn "Dryrun Delete"
opts (Options False True _) = putStrLn "Real Delete"
opts (Options True False destDir) = doDryRun destDir
opts (Options False False _) = putStrLn "Real Install"

main :: IO ()
main = do
  destDir <- lookupEnv "STOW_DIR"
  opts =<< execParser (optionInfo destDir) where
  optionInfo destDir = info (options destDir <**> helper)
    (fullDesc <> progDesc "STOW" <> header "a reimplementation of GNU Stow")
